import {TODO_ADD, TODO_DELETE, TODO_NEWTEXT_CHANGE} from '../stores/ActionType';

const sTodo = state => state.todo;
export const sTodoList = state => sTodo(state).todos;
export const sTodoNewText = state => sTodo(state).form.newTaskText;

const INITIAL_STATE = {
  todos: [],
  form: {
    newTaskText: '',
  },
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case TODO_NEWTEXT_CHANGE:
      return {
        ...state,
        form: {
          newTaskText: action.payload.value,
        },
      };
    case TODO_ADD:
      return {
        ...state,
        form: {
          newTaskText: '-',
        },
        todos: [
          ...state.todos,
          action.payload.todo,
        ],
      };
    case TODO_DELETE:
      return {
        ...state,
        todos: [
          ...(state.todos.filter(
              (item) => {item.id !== action.payload.todo.id;})),
        ],
      };
    default:
      return state;
  }
}
