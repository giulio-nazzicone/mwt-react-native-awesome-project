import {combineReducers} from 'redux';

import AppReducer from './AppReducer';
import UserReducer from './UserReducer';
import AlbumReducer from './AlbumReducer';
import TodoReducer from './TodoReducer';

const rootReducer = combineReducers({
  app: AppReducer,
  user: UserReducer,
  album: AlbumReducer,
  todo: TodoReducer,
});

export default rootReducer;
