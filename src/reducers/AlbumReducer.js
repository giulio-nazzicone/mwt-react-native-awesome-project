import {
  ALBUM_FETCH,
  ALBUM_FETCH_FAIL,
  ALBUM_FETCH_SUCCESS,
} from '../stores/ActionType';

const sAlbum = (state) => state.album;
export const sLoadedAlbums = state => sAlbum(state).albums;
export const sAlbumsLoading = state => sAlbum(state).loading;

const INITIAL_STATE = {
  albums: null,
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `${ALBUM_FETCH}_PENDING`:
      return {
        ...state,
        loading: true,
      };
    case `${ALBUM_FETCH}_REJECTED`:
      return {
        ...state,
        loading: false,
      };
    case `${ALBUM_FETCH}_FULFILLED`:
      return {
        ...state,
        albums: action.payload.albums,
        loading: false,
      };
    default:
      return state;
  }
}
