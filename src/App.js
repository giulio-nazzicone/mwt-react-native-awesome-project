import React, {useEffect} from 'react';
import {connect, useDispatch} from 'react-redux';
import {View, Platform, SafeAreaView} from 'react-native';
import firebase from 'firebase';
import SplashScreen from 'react-native-splash-screen';
import { SafeAreaProvider } from 'react-native-safe-area-context';


import {userLogin, userLogout} from './actions';
import RootNavigator from './navigator/RootNavigator';

const firebaseConfig = {
  apiKey: 'AIzaSyDSduPtbGhl6xm1RgzaIsSNheaYKbARqUc',
  authDomain: 'awesome-project-8c366.firebaseapp.com',
  databaseURL: 'https://awesome-project-8c366.firebaseio.com',
  projectId: 'awesome-project-8c366',
  storageBucket: 'awesome-project-8c366.appspot.com',
  messagingSenderId: '977115613335',
  appId: '1:977115613335:web:7f3cc622eb63db80fdc1d2',
};

class BackgroundService extends React.Component {
  constructor(props) {
    super(props);
    firebase.initializeApp(firebaseConfig);
    SplashScreen.hide();
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.props.userLogin(user);
      } else {
        this.props.userLogout();
      }
    });
  }
  render() {
    return null;
  }
}

export default App = function() {
  const dispatch = useDispatch();
  return (
      <>
        <BackgroundService
            userLogin={(user) => {dispatch(userLogin(user));}}
            userLogout={() => {dispatch(userLogout());}}
        />
        {/*<SafeAreaView style={styles.container}>*/}
        <SafeAreaProvider>
          <RootNavigator />
        </SafeAreaProvider>
        {/*</SafeAreaView>*/}
      </>
  );
};

    // if (loading) {
    //   return (
    //       <LoadingPage />
    //   );
    //
    //     <SafeAreaView style={styles.container}>
    //       {this.renderPage()}
    //     </SafeAreaView>

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'rgb(254, 178, 7)',
  },
};
