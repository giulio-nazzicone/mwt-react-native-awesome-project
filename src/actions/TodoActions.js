import {TODO_ADD, TODO_DELETE, TODO_NEWTEXT_CHANGE} from '../stores/ActionType';
import {sTodoNewText} from '../reducers/TodoReducer';

export function todoNewTaskTextChange(value) {
  return {
    type: TODO_NEWTEXT_CHANGE,
    payload: {
      value,
    },
  };
}

export function todoAdd() {
  return function(dispatch, getState) {
    const currentState = getState();
    const todo = {
      id: Math.floor(Math.random() * 10000),
      text: sTodoNewText(currentState),
    };
    dispatch({
      type: TODO_ADD,
      payload: {
        todo,
      },
    });
  };
}

export function todoRemove(todo) {
  return {
    type: TODO_DELETE,
    payload: {
      todo,
    },
  };
}
