import {
  ALBUM_FETCH,
  ALBUM_FETCH_FAIL,
  ALBUM_FETCH_SUCCESS,
} from '../stores/ActionType';
import * as API from '../api/Api'

export function albumFetched(albums) {
  return {
    type: ALBUM_FETCH_SUCCESS,
    payload: {
      albums
    }
  }
}
export function albumFetchOLD() {
  return function (dispatch) {
    dispatch({type: ALBUM_FETCH});
    API.getAlbums().
        then(albums => dispatch(albumFetched(albums))).
        catch(err => dispatch({type: ALBUM_FETCH_FAIL}));
  }
}

export function albumFetch() {
  return {
    type: ALBUM_FETCH,
    payload: API.getAlbums().then(r => ({albums: r}))
  };
}
