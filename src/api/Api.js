import axios from 'axios';

const URL_ALBUM = 'https://rallycoding.herokuapp.com/api/music_albums';

export const getAlbums = function () {
  return axios.get(URL_ALBUM).
      then(response => response.data).
      catch(error => {
        throw error;
      });
};
