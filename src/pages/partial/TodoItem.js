import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

export default function({todo, onPress}) {
  return (
      <View style={styles.itemContainer}>
        <View style={styles.textContainer}>
          <Text style={styles.text}>{todo.text}</Text>
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity style={styles.actionButton} onPress={onPress}>
            <View style={styles.actionButtonIcon} />
          </TouchableOpacity>
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {},
  text: {
    fontSize: 16,
  },
  actionContainer: {
    height: 200,
    backgroundColor: 'rgba(0,0,0,0.1)',
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionButton: {
    height: 45,
    justifyContent: 'center',
    paddingHorizontal: 12.5,
  },
  actionButtonIcon: {
    height: 20,
    width: 20,
    backgroundColor: 'red',
  },
});

