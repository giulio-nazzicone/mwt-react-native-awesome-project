import React from 'react';
import {View, Text, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useSafeArea} from 'react-native-safe-area-context';

import {ListButton, Card, CardItem, Input} from '../components';
import {sTodoList, sTodoNewText} from '../reducers/TodoReducer';
import {todoRemove} from '../actions/TodoActions';
import TodoItem from './partial/TodoItem';

export default function({navigation}) {
  const todolist = useSelector(sTodoList);

  const dispatch = useDispatch();
  const inset = useSafeArea();
  return (
      <View style={[styles.pageContainer, {paddingBottom: inset.bottom}]}>
        <FlatList
            style={{paddingTop: 10, flex: 1}}
            data={todolist}
            ListEmptyComponent={() => <Text>Nessun elemento</Text>}
            keyExtractor={todo => `todo-${todo.id}`}
            renderItem={({item: todo, index, separator}) => (
                <Card>
                  <CardItem noMargin>
                    <TodoItem todo={todo} onPress={() => {dispatch(todoRemove(todo));}}/>
                  </CardItem>
                </Card>
            )}
        />
      </View>
  );
}

const styles = {
  pageContainer: {
    flex: 1,
  },
};
