import React, {Component, useEffect} from 'react';
import axios from 'axios';
import {View, Text, ActivityIndicator, ScrollView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import firebase from 'firebase';

import AlbumItem from './partial/AlbumItem';
import {Card, CardItem, LoginButton} from '../components';
import {sAlbumsLoading, sLoadedAlbums} from '../reducers/selectors';
import {albumFetch, albumFetched} from '../actions';

const URL_ALBUM = 'https://rallycoding.herokuapp.com/api/music_albums';

export default function() {
  const albums = useSelector(sLoadedAlbums);
  const loading = useSelector(sAlbumsLoading);
  const dispatch = useDispatch();


  useEffect(() => {
    dispatch(albumFetch());
  }, []);

  if (loading || albums === null) {
    return (
        <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size={'large'} color={'green'} />
        </View>
    );
  }
  if (albums.length === 0) {
    return <Text>Empty Albums List</Text>;
  }

  return (
      <View style={styles.container}>
        <ScrollView>
          <Card>
            {albums.map((album, key) => (
                <AlbumItem
                    key={`album-item-${album.title.replace(/ /g, '-').toLowerCase()}`}
                    album={album}
                />
            ))}
          </Card>
          <Card>
            <CardItem>
              <LoginButton onPress={() => {
                firebase.auth().signOut();
              }} text={'logout'}/>
            </CardItem>
          </Card>
        </ScrollView>
      </View>
  );
};

const styles = {
  container: {
    flex: 1,
    paddingVertical: 15,
  },
};
