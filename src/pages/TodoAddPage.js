import React from 'react';
import {View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useSafeArea} from 'react-native-safe-area-context';

import {ListButton, Card, CardItem, Input} from '../components';
import {sTodoList, sTodoNewText} from '../reducers/TodoReducer';
import {todoAdd, todoNewTaskTextChange} from '../actions/TodoActions';

export default function() {
  // const todolist = useSelector(sTodoList);
  const newTaskText = useSelector(sTodoNewText);
  const dispatch = useDispatch();
  const inset = useSafeArea();
  return (
      <View style={[styles.pageContainer, {paddingBottom: inset.bottom}]}>
        <View style={{paddingTop: 10}}>
          <Card>
            <CardItem>
              <Text>Add a Task</Text>
            </CardItem>
            <CardItem>
              <Input
                  placeholder={'Text of task'}
                  value={newTaskText}
                  handleChangeText={(value) => {
                    dispatch(todoNewTaskTextChange(value));
                  }}
              />
            </CardItem>
          </Card>
        </View>
        <View>
          <Card>
            <CardItem noMargin>
              <ListButton text={'Add Task'}
                          onPress={() => {dispatch(todoAdd());}} />
            </CardItem>
          </Card>
        </View>
      </View>
  );
}

const styles = {
  pageContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
};
