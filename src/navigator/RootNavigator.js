import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {useSelector} from 'react-redux';

import AlbumListPage from '../pages/AlbumListPage';
import LoginPage from '../pages/LoginPage';
import SignUpPage from '../pages/SignUpPage';
import TodoNavigator from './TodoNavigator';

import {sAppLogged} from '../reducers/selectors';

const RootStack = createStackNavigator();

export default function() {
  const logged = useSelector(sAppLogged);
  return (
      <NavigationContainer>
        <RootStack.Navigator>
          {!logged && (
              <>
                <RootStack.Screen name={'SignIn'} component={LoginPage}/>
                <RootStack.Screen name={'SignUp'} component={SignUpPage}/>
              </>
          )}
          {logged && (
              <>
                <RootStack.Screen name={'Todo'} component={TodoNavigator}/>
                <RootStack.Screen name={'Album'} component={AlbumListPage}/>
              </>
          )}
        </RootStack.Navigator>
      </NavigationContainer>
  );
}
