import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TodoListPage from '../pages/TodoListPage';
import TodoAddPage from '../pages/TodoAddPage';

const TodoStack = createDrawerNavigator();

export default function() {
  return (
      <TodoStack.Navigator>
        <TodoStack.Screen name={'TodoList'} component={TodoListPage} />
        <TodoStack.Screen name={'TodoAdd'} component={TodoAddPage} />
      </TodoStack.Navigator>
  );
}
